require('dotenv').config()
const express = require('express');
const mysql = require('mysql')
const bodyParser = require('body-parser');

const app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

const connection = mysql.createConnection({
	host: process.env.DB_HOST,
	user: process.env.DB_USER,
	password: process.env.DB_PASS,
	database: process.env.DB_NAME
});

connection.connect((err) => {
	if (err) {
		throw err;
	}

	console.log('Mysql is ready to use!')
})

const lblErr = "Oops.. terjadi kesalahan, mohon coba lagi"

app.get('/api/products', (req, res) => {
	let q = "SELECT * FROM toko"
	let exec = connection.query(q, (err, results) => {
		try {
			if (err) {
				throw err
			}

			return res.status(200).json({
				status: "success",
				success: true,
				data: results
			})
		} catch (e) {
			return res.status(400).json({
				status: "error",
				success: false,
				message: lblErr
			})
		}
	})
})

app.get('/api/expensive-products', (req, res) => {
	let q = "SELECT * FROM toko WHERE CAST(harga_produk AS UNSIGNED ) > 80000"
	let exec = connection.query(q, (err, results) => {
		try {
			if (err) {
				throw err
			}

			return res.status(200).json({
				status: "success",
				success: true,
				data: results
			})
		} catch (e) {
			return res.status(400).json({
				status: "error",
				success: false,
				message: lblErr
			})
		}
	})
})

app.get('/api/product/:id', (req, res) => {
	let q = `SELECT * FROM toko WHERE id_produk=${req.params.id}`
	let exec = connection.query(q, (err, result) => {
		try {
			if (err) {
				throw err
			}
			if (result.length === 0) {
				return res.status(400).json({
					status: "error",
					success: false,
					message: "Produk tidak ditemukan."
				})
			}

			return res.status(200).json({
				status: "success",
				success: true,
				data: result[0]
			})
		} catch (e) {
			return res.status(400).json({
				status: "error",
				success: false,
				message: lblErr
			})
		}
	})
})

app.post('/api/product/create', (req, res) => {
	const { harga_produk = '', nama_produk = ''} = req.body
	if (harga_produk === '' || nama_produk === '') {
		return res.status(400).json({
			status: "error",
			success: false,
			message: "Semua kolom wajib diisi (Nama Produk, Harga Produk)."
		})
	}
	let q = `INSERT INTO toko (nama_produk, harga_produk) VALUES ("${nama_produk}", "${harga_produk}")`
	let exec = connection.query(q, (err, result) => {
		try {
			if (err) {
				throw err
			}

			if (result.affectedRows === 0) {
				return res.status(400).json({
					status: "error",
					success: false,
					message: "Gagal menambahkan produk."
				})
			}

			return res.status(200).json({
				status: "success",
				success: true,
				message: "Berhasil menambahkan produk."
			})
		} catch (e) {
			return res.status(400).json({
				status: "error",
				success: false,
				message: lblErr
			})
		}
	})
})

app.post('/api/product/:id/update', (req, res) => {
	const id = req.params.id
	const { harga_produk = '', nama_produk = ''} = req.body

	let arr = []
	if (harga_produk !== '') {
		arr.push(`harga_produk="${harga_produk}"`)
	}
	if (nama_produk !== '') {
		arr.push(`nama_produk="${nama_produk}"`)
	}

	let q = `UPDATE toko SET ${arr.join(',')} WHERE id_produk=${id}`
	let exec = connection.query(q, (err, result) => {
		try {
			if (err) {
				throw err
			}

			if (result.affectedRows === 0) {
				return res.status(400).json({
					status: "error",
					success: false,
					message: "Gagal memperbarui produk, produk tidak ditemukan."
				})
			}

			return res.status(200).json({
				status: "success",
				success: true,
				message: "Berhasil memperbarui produk."
			})
		} catch (e) {
			return res.status(400).json({
				status: "error",
				success: false,
				message: lblErr
			})
		}
	})
})

app.post('/api/product/:id/delete', (req, res) => {
	let q = `DELETE FROM toko WHERE id_produk=${req.params.id}`
	let exec = connection.query(q, (err, result) => {
		try {
			if (err) {
				throw err
			}

			if (result.affectedRows === 0) {
				return res.status(400).json({
					status: "error",
					success: false,
					message: "Gagal menghapus produk, produk tidak ditemukan."
				})
			}

			return res.status(200).json({
				status: "success",
				success: true,
				message: "Berhasil menghapus produk."
			})
		} catch (e) {
			return res.status(400).json({
				status: "error",
				success: false,
				message: lblErr
			})
		}
	})
})

app.get('/api/biodata', (req, res) => {
	return res.json( {
			    nama: "Elang Putra Sartika",
				jenis_kelamin: "Laki-laki",
				no_telepon: "082196560301",
				email: "elangputrasartika@gmail.com",
				alamat: "Jl Kaliurang km 14, Kos Usaha Mulia",
				tgl_lulus: "02/04/2020",
				domisili: "Yogyakarta"
			})
})

app.post('/api/check-pass', (req, res) => {
	let { password = '', confirm_password = '' } = req.body
	if (password === '' || confirm_password === '') {
		return res.status(400).json({
			status: "error",
			success: false,
			message: "Semua kolom wajib diisi (Password, Konfirmasi Password)."
		})
	}
	if (password.length < 8 || password.length > 12) {
		return res.status(400).json({
			status: "error",
			success: false,
			message: "Password harus terdiri dari 8 - 12 karakter."
		})
	}
	if (password !== confirm_password) {
		return res.status(400).json({
			status: "error",
			success: false,
			message: "Kolom password dan konfirmasi password harus sama."
		})
	}

	return res.status(200).json({
		status: "success",
		success: true,
		message: "Password anda sudah baik."
	})
})

app.listen(process.env.PORT, () => {
	console.log(`Server started on port ${process.env.PORT}`)
})
